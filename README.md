# The Vivid Programming Language

<img src="./assets/logo.png" alt="Vivid logo"  width=50% height=50%>

### A custom interpreted programming language written in [Python 3](https://python.org).

>### Document last updated 2023.06.05

## About

Vivid-Lang is intended to be a simple programming language. The project utilises lexical analysis to generate tokens from a given source code, then parses the tokens to form a parse tree. The parse tree is interpreted and ran as Python code.

The purpose of this project is to provide insight into how interpreted and compiled programming languages parse human-readable code and execute it. Further steps could be taken to convert plaintext source code into bytecode or machine code.

I spent about three weeks developing this project and it is currently **incomplete**.

## Example
See the [example script](./src/example.vv) written in the Vivid programming language for an idea of the proper syntax.
```rs
#!/usr/bin/env -S python -m vivid -f

fn product(integer x, integer y) -> integer {
    // Multiply two numbers.
    return x * y;
}

fn main() -> void {
    // Main method.
    integer x <- 7;
    integer y <- 5;
    integer z <- product(x, y);
    println(z);
}
```

## Installation
In order to run the project, all project requirements and dependencies must be met.

### 1. Download the repository.

Clone the Git repository hosted on [GitLab](https://gitlab.com/lunardev/vivid-lang.git).
```bash
$ git clone git@gitlab.com:lunardev/vivid-lang.git
$ cd vivid-lang
$ git checkout final
```
Alternatively, download the [project zip file](https://gitlab.com/lunardev/vivid-lang/-/archive/final/vivid-lang-final.zip) if the `git` binary is not available on the system.
```bash
$ curl https://gitlab.com/lunardev/vivid-lang/-/archive/final/vivid-lang-final.zip -o vivid-lang-final.zip
$ unzip vivid-lang-final.zip
$ shred -u vivid-lang-final.zip
$ cd vivid-lang-final
```
### 2. Install the project dependencies.
Python package dependencies are defined in [requirements.txt](./requirements.txt). It is recommended to use a virtual environment to avoid package conflicts.

Setting up a virtual environment using `venv` on *nix:
```bash
$ python3 -m venv .venv
$ source .venv/bin/activate
$ python3 -m pip install -r requirements.txt
```

Setting up a virtual environment using `venv` on Windows:
```batch
$ py -m venv .venv
$ .venv\Scripts\activate
$ py -m pip install -r requirements.txt
```


## Usage

### 1. Running the project.
The minimum supported Python version for this project is `Python 3.11`. Check the version of Python installed on your system using the following command:
```bash
$ python3 --version
Python 3.11.3
```

The project must be ran from the [./src](./src/) directory.
```bash
$ cd ./src
$ python3 -m vivid
```

### 2. Command line arguments

Enter the Vivid REPL shell using the `--repl` flag:
```bash
$ cd ./src
$ python3 -m vivid --repl
```

Run a Vivid (*.vv) file via the `--file` flag:
```bash
$ python3 -m vivid --file ./example.vv
```

Alternatively, files support the shebang line `#!/usr/bin/env -S python3 -m vivid -f`. Note that the file can only be ran while the current working directory is [./src](./src/).
```bash
# Make the file executable.
$ chmod +x ./example.vv
# Run the file.
$ ./example.vv
```

A list of all available command line parameters:
| Command     | Short   | Description                       |
|-------------|---------|-----------------------------------|
| --help      | -h      | Display the program help message. |
| --version   | -v      | Display the Vivid version code.   |
| --repl      | -r      | Enter the REPL shell.             |
| --file PATH | -f PATH | Run Vivid code from a file.       |

### 3. Vivid Language Syntax
| Token      | Type       | Lexeme                 | Regular expression |
|------------|------------|------------------------|--------------------|
| shebang    | comment    | #!/usr/bin/env python3 | ^#[^\r\n]*         |
| comment    | comment    | // Comment body.       | [\\/]{2}[^\r\n]*   |
| let        | keyword    | let                    | \b(let)\b          |
| constant   | keyword    | const                  | \b(const)\b        |
| boolean    | boolean    | true                   | \b(true\|false)\b  |
| identifier | identifier | variable_name          | [a-zA-Z_]\w*       |
| equality   | binary_op  | ==                     | [=]{2}             |
| plus       | binary_op  | +                      | [+]                |

For a complete list of all valid token types, refer to [tokens.py](./src/vivid/tokeniser/tokens.py)

## Project Documentation

The Vivid-Lang project has been modularised into four separate components: the [tokeniser](./src/vivid/tokeniser), [terminal](./src/vivid/terminal), [errors](./src/vivid/errors), and [parser](./src/vivid/parser) modules.

### The Goal
Before describing the purpose of each module, it is important to understand the purpose and process of the project as a whole. We start off with a file written in the Vivid programming language. How can we get this file to be executed like a Python script? First, the contents of the file must be read, parsed and verified. Language parsers require specific grammar in order to correctly interpret the instructions that it has been given. After the file input has been validated, a logic path (or in more complex cases a logic tree) must be built from the code. A well-known example of a logic path is the [order of operations](https://wiki.metastem.su/#/wiki/Order%20of%20operations) rules (PEMDAS) from mathematics. The logic tree is a lower-level abstraction of the readable Vivid source code, but is easier for the interpreter to understand. From there, the tree can be executed recursively, resulting in a form of program execution without compilation to machine code.

To summarise, this project must:
1. Read the file.
2. Parse the content of the file.
3. Validate the code syntax.
4. Create a logic tree from the code.
5. Execute the logic tree and return an output.

There are further details even within this, such as defining complex language grammar (binary/unary operators, scopes, namespaces, etc.), exception handling, and compilation into machine code (which was not implemented in this project).

### [The Tokeniser Module](./src/vivid/tokeniser)
In order to do anything with the source code, it must be converted into a parsable format. This is where the magic of [lexical analysis](https://wiki.metastem.su/#/wiki/Lexical%20analysis) comes in. Using regular expressions, the source code is separated into lexical token objects that identify an actual meaning to the string. For example, when the Python interpreter is reading a Python script file, "print" is just a string. However, after lexical analysis, the lexical token object `PRINT` identifies that the "print" string represents a callable method. Lexical tokens can also add additional context to the string token, such as the type of token (operator, name, or datatype), start/end index of the string, or the scope of the identifier. The purpose of a lexer is to convert the source code string into a contextualised string of lexical tokens.

Lexical analysis is powerful, as the lexical token string allows for advanced text parsing. Syntax highlighting in an IDE, reading data from a JSON file, and even a grammar/spell checking tool all utilise lexical analysis to some degree.

**Example**:<br/>
The tokeniser takes simple input code written in Vivid:
```rust
// main.vv
fn main() -> void {
    println("Hello, world!");
}
```

The lexer returns a list of lexical tokens. Whitespace and comment lexemes do not affect the code and are ignored:
```
Token              Type                 Lexeme
----------------------------------------------
METHOD             KEYWORD              fn
IDENTIFIER         IDENTIFIER           main
PAREN_OPEN         SCOPE_OPEN           (
PAREN_CLOSE        SCOPE_CLOSE          )
RETURN_TYPE        SYNTAX               ->
TYPE_VOID          DATATYPE             void
CURLY_OPEN         SCOPE_OPEN           {
PRINT_LINE         BUILTIN              println
PAREN_OPEN         SCOPE_OPEN           (
STRING             STRING               "Hello, world!"
PAREN_CLOSE        SCOPE_CLOSE          )
SEMICOLON          SYNTAX               ;
CURLY_CLOSE        SCOPE_CLOSE          }
```

- For the lexical analysis code in this project, view [lexer.py](./src/vivid/tokeniser/lexer.py).
- For lexical token definitions, view [tokens.py](./src/vivid/tokeniser/tokens.py).


### [The Terminal Module](./src/vivid/terminal)
The Python programming language has an interactive shell that will prompt for user input, evaluate the code, and display the returned value. This is known as a read-eval-print loop, or REPL. The terminal provides a similar experience, except it will return a list of lexical tokens. A REPL instance is quite simple, but it provides invaluable opportunity for debugging and testing the code parser.

![Example REPL output](./assets/screenshots/repl.png)

- For the REPL implementation, view [repl.py](./src/vivid/terminal/repl.py).
- To see the ANSI escape sequences used, view [display.py](./src/vivid/terminal/display.py).

### [The Errors Module](./src/vivid/errors)
If a problem is ever encountered during the tokenisation or parsing process, the interpreter should raise a relevant exception and display code traceback. The process is stopped instead of continuing because is best to not assume what the user what the user is trying to do. When an exception is raised, parsing of the code stops and an error message is displayed. The error message includes the line and column the exception occured on, a snippet of the code that caused the error, and a short sentance describing the nature of the exception. In a real language interpreter, the exception traceback is very helpful for finding mistakes or bugs.

![Example exception output](./assets/screenshots/exception.png)

- All custom exception classes used are defined in [exceptions.py](./src/vivid/errors/exceptions.py)

### [The Parser Module](./src/vivid/parser)
Now that the interpreter has a parsable list of lexical tokens thanks to the `tokeniser` module, the parsing can commence. The parser defienes the actual grammar and syntax of the language. It determines what is considered a valid line of code and what is invalid. For example a language lexer would consider the string "`my ant for. building Centre`" to be a valid English string, but the parser would raise an exception because it has improper grammar. This is why the parser is imporant; it enforces the grammar and is what ensures that the input is an actual language. Once the input has been validated by the parser, a syntax tree will be built from the lexical token string. The tree will determine what each token or group of tokens will actually "do" when evaluated, as well as the proper order of execution of tokens.

> The Parser module has not been completed as of the last update to this document.
