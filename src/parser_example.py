#!/usr/bin/env python

"""
Working grammar/parse tree example.

Given more time, this concept would be integrated into the project.
Inspired by "Coding Trees in Python" by Computerphile.
Source: https://y.com.sb/7tCNu4CnjVc.
"""

from __future__ import annotations

from abc import ABC, abstractmethod
from dataclasses import dataclass


@dataclass(frozen=True)
class Value:
    """Value tree node."""

    value: str | int | float

    def __str__(self) -> str:
        """Get string representation of Value."""
        return f"{self.value}"

    def eval(self, environment: dict[str, int | float]) -> int | float:
        """Get environment variable."""
        if isinstance(self.value, str):
            return environment[self.value]
        else:
            return self.value


@dataclass(frozen=True)
class Number(Value):
    """Number tree node."""

    value: int | float

    def __repr__(self) -> str:
        """Return object representation as a string."""
        return f"{type(self).__name__}({self.value})"


@dataclass(frozen=True)
class Variable(Value):
    """Variable tree node."""

    value: str

    def __repr__(self) -> str:
        """Return object representation as a string."""
        return f"{type(self).__name__}({self.value})"


@dataclass(frozen=True)
class Expression(ABC):
    """Expression tree node."""


@dataclass(frozen=True)
class Operation(Expression):
    """Base operator tree node."""

    @abstractmethod
    def get_operator(self) -> str:
        """Return operator symbol."""

    @abstractmethod
    def eval(self, environment: dict[str, int | float]) -> int | float:
        """Evaluate operator."""


@dataclass(frozen=True)
class BinaryOperator(Operation):
    """Binary operator tree node."""

    left_node: Value | Operation
    right_node: Value | Operation

    def __str__(self) -> str:
        """Return string representation of BinOp."""
        return f"({self.left_node} {self.get_operator()} {self.right_node})"

    @abstractmethod
    def get_operator(self) -> str:
        """Return operator symbol."""

    @abstractmethod
    def eval(self, environment: dict[str, int | float]) -> int | float:
        """Evaluate operator."""


@dataclass(frozen=True)
class UnaryOperator(Operation):
    """Unary operator tree node."""

    operand: Value | Operation

    def __str__(self) -> str:
        """Return string representation of UnOp."""
        return f"({self.get_operator()}{self.operand})"

    @abstractmethod
    def get_operator(self) -> str:
        """Return operator symbol."""

    @abstractmethod
    def eval(self, environment: dict[str, int | float]) -> int | float:
        """Evaluate operator."""


@dataclass(frozen=True)
class Plus(BinaryOperator):
    """Addition operator tree node."""

    def __repr__(self) -> str:
        """Return object representation as a string."""
        return f"{type(self).__name__}({self.left_node!r}, {self.right_node!r})"

    def get_operator(self) -> str:
        """Return operator symbol."""
        return "+"

    def eval(self, environment: dict[str, int | float]) -> int | float:
        """Evaluate operator."""
        return self.left_node.eval(environment) + self.right_node.eval(environment)


@dataclass(frozen=True)
class Minus(BinaryOperator):
    """Subtraction operator tree node."""

    def __repr__(self) -> str:
        """Return object representation as a string."""
        return f"{type(self).__name__}({self.left_node!r}, {self.right_node!r})"

    def get_operator(self) -> str:
        """Return operator symbol."""
        return "-"

    def eval(self, environment: dict[str, int | float]) -> int | float:
        """Evaluate operation."""
        return self.left_node.eval(environment) - self.right_node.eval(environment)


@dataclass(frozen=True)
class Multiply(BinaryOperator):
    """Multiplication operator tree node."""

    def __repr__(self) -> str:
        """Return object representation as a string."""
        return f"{type(self).__name__}({self.left_node!r}, {self.right_node!r})"

    def get_operator(self) -> str:
        """Return operator symbol."""
        return "*"

    def eval(self, environment: dict[str, int | float]) -> int | float:
        """Evaluate operation."""
        return self.left_node.eval(environment) * self.right_node.eval(environment)


@dataclass(frozen=True)
class Divide(BinaryOperator):
    """Division operator tree node."""

    def __repr__(self) -> str:
        """Return object representation as a string."""
        return f"{type(self).__name__}({self.left_node!r}, {self.right_node!r})"

    def get_operator(self) -> str:
        """Return operator symbol."""
        return "/"

    def eval(self, environment: dict[str, int | float]) -> float:
        """Evaluate operation."""
        return self.left_node.eval(environment) / self.right_node.eval(environment)


def main() -> None:
    """Entrypoint to the Python script."""
    environment: dict[str, int | float]
    environment = {"x": 2, "y": 3}

    # Node notation for the expression "3 * (x + y)".
    expression = Multiply(Number(3), Plus(Variable("x"), Variable("y")))
    evaluation = expression.eval(environment)
    print(f"{expression!r}\n{expression}\n{evaluation}")


if __name__ == "__main__":
    main()
