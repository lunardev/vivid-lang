# Parser Module
### Parses Vivid tokens using grammar/syntax rules.

## Files

__./src/vivid/parser/__<br/>
├── [\_\_init__.py](./__init__.py)<br/>
│．└── _Initialises the module._<br/>
├── [parse_tree.py](./parse_tree.py)<br/>
│．└── _Creates a parse tree from Vivid code tokens._<br/>
└── [grammar.txt](./grammar.txt)<br/>
．．└── _A rough guideline of the language grammar & syntax._<br/>

> Note: This module is incomplete.
