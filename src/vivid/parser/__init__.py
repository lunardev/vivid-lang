"""Parse Vivid tokens using syntax/grammar rules."""

from vivid.parser.parse_tree import generate_ast

# The identifiers accessible via a wildcard import (from vivid.parser import *).
__all__ = ["generate_ast"]
