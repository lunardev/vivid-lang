"""Parse Vivid tokens using grammar/syntax rules."""

from __future__ import annotations

from sys import stderr
from traceback import format_exc
from typing import TYPE_CHECKING

from vivid.errors import BadTypeError, InvalidSyntaxError, VividError
from vivid.tokeniser.tokens import TokenEnum, TokenType

if TYPE_CHECKING:
    from vivid.tokeniser import Token, TokenIterator


class Parser:
    """
    Main parser class.

    Inspiration: https://code.whatever.social/questions/25049751.
    """

    token: Token | None

    def __init__(self, src: str, file: str, tokens: TokenIterator) -> None:
        """Define class instance attributes."""
        self.src = src
        self.file = file
        self.old = tokens
        self.tokens = tokens
        self.token = None

    def _advance(self) -> bool:
        """Iterate to the next token. Return False if there are no more tokens."""
        try:
            self.token = next(self.tokens)
        except StopIteration:
            return False
        else:
            return self.token is not None

    def _reset_iter(self) -> None:
        self.tokens = self.old

    def advance_token(self, *tokens: TokenEnum) -> bool:
        """Advance to the next token and return boolean success status."""
        peek = self.tokens.peek
        if peek is not None and peek.token in tokens:
            self._advance()
            return True
        else:
            return False

    def advance_token_type(self, *tokens: TokenType) -> bool:
        """Advance to the next token and return boolean success status."""
        peek = self.tokens.peek
        if peek is not None and peek.type in tokens:
            self._advance()
            return True
        else:
            return False

    def expression(self) -> bool:
        """
        Highest level of the grammar tree.

        Return True if the expression is valid, else False.
        Grammar: ASSIGNMENT
        """
        return self.assignment()  #  or self.method() or self.keyword()

    def assignment(self) -> bool:
        """
        Define grammar for variable assignment.

        Grammar: TYPE IDENTIFIER "<-" SYMBOL ";"
        Code example: `integer my_number <- 1;`
        """
        type_token = Token
        self._reset_iter()

        # Check for the variable assignment type annotation.
        if not self.advance_token(
            TokenEnum.TYPE_INTEGER,
            TokenEnum.TYPE_FLOAT,
            TokenEnum.TYPE_BOOLEAN,
            TokenEnum.TYPE_STRING,
        ):
            type_annotation = False
        else:
            type_annotation = True
            assert self.token is not None
            type_token = self.token

        # Check for an identifier (the variable name).
        if not self.identifier():
            return False

        # Check for the assignment operator.
        if not self.assign():
            return False

        # It is now safe to raise an exception because we know any syntax after
        # the assignment operator pertains to variable assignment.
        if not type_annotation:
            assert self.token is not None
            raise BadTypeError(self.src, self.file, self.token.position, VividError.NO_TYPE)
        if not self.symbol():
            assert self.token is not None
            raise InvalidSyntaxError(self.src, self.file, self.token.position, VividError.INCOMPLETE)
        else:
            token = self.token
            assert token is not None
            # Raise an exception if the type annotation doesn't match the variable type.
            if token.type != type_token.type:
                raise BadTypeError(
                    self.src,
                    self.file,
                    token.position,
                    VividError.BAD_TYPE.format(token.type.name, type_token.type.name),
                )
        # Check for a semicolon that indicates the end of a complete statement.
        if not self.semicolon():
            assert self.token is not None
            raise InvalidSyntaxError(self.src, self.file, self.token.position, VividError.SEMICOLON)

        return True

    def identifier(self) -> bool:
        """Define identifier token grammar check."""
        res = self.advance_token(TokenEnum.IDENTIFIER)
        return res

    def assign(self) -> bool:
        """Define assignmnet token grammar check."""
        res = self.advance_token(TokenEnum.ASSIGN)
        return res

    def symbol(self) -> bool:
        """
        Define symbol grammar.

        Grammar: (TERM | VALUE | IDENTIFIER)
        """
        return self.term() or self.value() or self.identifier()

    def semicolon(self) -> bool:
        """Define semicolon token grammar check."""
        res = self.advance_token(TokenEnum.SEMICOLON)
        return res

    def method(self) -> bool:
        """
        Define method/function grammar.

        Grammar: (METHOD_DEF | METHOD_CALL)
        """
        self._reset_iter()
        return self.method_def() or self.method_call()

    def method_def(self) -> bool:
        """
        Define method/function definition grammar.

        Grammar: "fn" IDENTIFIER "(" ")"
        Code example: `fn method()`
        """
        if not self.advance_token(TokenEnum.METHOD):
            return False
        if not self.identifier():
            raise Exception
        if not self.advance_token(TokenEnum.PAREN_OPEN):
            raise Exception
        if not self.advance_token(TokenEnum.PAREN_CLOSE):
            assert self.token is not None
            raise InvalidSyntaxError(self.src, self.file, self.token.position, VividError.INCOMPLETE)

        return True

    def method_call(self) -> bool:
        """
        Define method/function call grammar.

        Grammar: IDENTIFIER "(" ")" ";"
        Code example: `method();`
        """
        if not (self.identifier() or self.keyword()):
            return False
        if not self.advance_token(TokenEnum.PAREN_OPEN):
            return False
        if not self.advance_token(TokenEnum.PAREN_CLOSE):
            assert self.token is not None
            raise InvalidSyntaxError(self.src, self.file, self.token.position, VividError.INCOMPLETE)
        if not self.semicolon():
            assert self.token is not None
            raise InvalidSyntaxError(self.src, self.file, self.token.position, VividError.SEMICOLON)

        return True

    def keyword(self) -> bool:
        """
        Define builtin/keyword tree node.

        Grammar: (BUILTIN | KEYWORD)
        Code example: `println`
        """
        self._reset_iter()

        res = self.advance_token_type(TokenType.BUILTIN, TokenType.KEYWORD)
        return res

    def term(self) -> bool:
        """
        Define term/expression grammar (not implemented).

        Grammar: (VALUE OPERATOR VALUE | OPERATOR VALUE)
        Grammar: (BINARY_OP | UNARY_OP)
        Code example: `(5 - 3)`
        """
        raise NotImplementedError

    def value(self) -> bool:
        """
        Define a value tree node; a raw datatype value.

        Grammar: (INTEGER | FLOAT | STRING)
        Code example: `3.14`
        """
        res = self.advance_token(TokenEnum.INTEGER, TokenEnum.FLOAT, TokenEnum.STRING)
        return res


def generate_ast(src: str, file: str, tokens: TokenIterator) -> bool | None:
    """Generate a parse tree expression using parse grammar."""
    try:
        # Create a parser object and return the tree expression.
        parser = Parser(src, file, tokens)
        ast = parser.expression()
    except VividError as error:
        # Write Vivid exception traceback to stderr.
        print(error.traceback(), file=stderr)
        return None
    except Exception:
        # Debug: write unexpected exception traceback to stderr.
        print(end=format_exc(), file=stderr)
        return None
    else:
        return ast
