"""Vivid-Lang: a custom interpreted programming language written in Python 3."""

from sys import version_info

from colorama import Fore, init


def minimum_python_version() -> bool:
    """Check if the current Python version matches the minimum supported version."""
    return version_info >= (3, 11, 0)


# Initialise the colorama module.
init(autoreset=True)

if not minimum_python_version():
    print(f"{Fore.RED}Minimum supported Python version is 3.11.0.")
    raise SystemExit(1)
