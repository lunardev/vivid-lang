"""Run the project via `$ python3 -m vivid` from the src directory."""

import sys
from argparse import ArgumentParser
from dataclasses import dataclass
from pathlib import Path

from colorama import Fore, Style

from vivid.terminal import Ansi, enter_repl
from vivid.tokeniser import Lexer

# Define project metadata.
__version__ = "0.1.0"
__author__ = "T. Rodriguez"
__remote__ = "https://gitlab.com/lunardev/vivid-lang"
__license__ = "MIT (./LICENSE)"


@dataclass(frozen=True)
class VividArgs:
    """Dataclass of typed command line parameters."""

    version: bool
    repl: bool
    path: Path


def get_usage() -> str:
    """Return a string describing Vivid program usage."""
    binary = Path(sys.executable).absolute()
    module = Path(__file__).parent.name
    return f"{Style.BRIGHT}{binary} -m {module} [--help] [--version] [--repl | --file PATH]{Style.RESET_ALL}"


def parse_file(file: str) -> Path:
    """
    Parse a given file to ensure it is a valid path.

    Exit the program if the file can not be read, else return a Path object.
    """
    path = Path(file).expanduser()

    # Check that the provided path exists on the system.
    try:
        # Attempt to resolve the given path.
        path = path.resolve(strict=True)
        # Raises an error if the path is a directory; a directory can not be read.
        _ = path.read_text(encoding="utf-8")
    except FileNotFoundError:
        print(f'{Fore.RED}Path "{path.name}" does not exist.', file=sys.stderr)
    except IsADirectoryError:
        print(f'{Fore.RED}Path "{path.name}" is a directory, not a file.', file=sys.stderr)
    except UnicodeDecodeError:
        print(f'{Fore.RED}File "{path.name}" uses an unsupported text encoding.', file=sys.stderr)
    else:
        return path

    # Exit the process with exit code 1.
    raise SystemExit(1)


def argument_parser() -> ArgumentParser:
    """
    Define the program's CLI parameters using argparse.

    Using the argparse module is better than parsing command line
    arguments by reading sys.argv manually, as it is easier to use
    and provides more features. There is no point in wasting time
    "reinventing the wheel".
    """
    parser = ArgumentParser(
        description=f"{Fore.LIGHTGREEN_EX}Vivid-Lang{Fore.RESET} ({__version__}): A custom interpreted programming language written in Python 3.",
        epilog=f"Coded with {Fore.LIGHTRED_EX}♥{Fore.RESET} by {__author__}",
        usage=get_usage(),
    )
    group = parser.add_mutually_exclusive_group()
    # Create program parameters.
    parser.add_argument("-v", "--version", action="store_true", help="display Vivid version number")
    group.add_argument("-r", "--repl", action="store_true", help="enter the REPL shell")
    group.add_argument(
        "-f", "--file", dest="path", type=parse_file, help="run Vivid code from a file"
    )
    return parser


def main() -> int:
    """Entrypoint to the Vivid-Lang Python program."""
    status = 0
    # Parse any parameters passed via the command line.
    parser = argument_parser()
    raw_args = vars(parser.parse_args())
    # Pass arguments to VividArgs to ensure the correct attributes are used.
    args = VividArgs(**raw_args)

    if args.version:
        # Display version information.
        print(f"Vivid {__version__}")
    elif args.repl:
        # Enter the REPL shell.
        status = enter_repl(
            version=__version__,
            author=__author__,
            remote=f"{Fore.LIGHTBLUE_EX}{Ansi.UNDERLINE}{__remote__}{Style.RESET_ALL}",
            license=__license__,
        )
    elif args.path is not None:
        # Read the file.
        source = args.path.read_text(encoding="utf-8")
        # Tokenise the file contents.
        lexer = Lexer(source, args.path.name)
        # Display traceback if an error occured.
        status = lexer.debug_tokens()
    else:
        # Print program usage.
        parser.print_help(sys.stderr)

    return status


# Exit the program from a single point.
exit_code = main()
raise SystemExit(exit_code)
