"""Simple methods to make regular expressions easier."""


def match_or(*regex: str) -> str:
    """Join multiple regular expressions using the logical OR operator (no grouping)."""
    return "|".join(regex)


def group(*regex: str) -> str:
    """Group multiple regular expressions."""
    return "(" + match_or(*regex) + ")"


def match_any(*regex: str) -> str:
    """Match any of the regular expressions."""
    return group(*regex) + "*"


def maybe(*regex: str) -> str:
    """Match zero or one of the regular expressions."""
    return group(*regex) + "?"


def exact(*regex: str) -> str:
    """Match an exact regular expression string."""
    return rf"\b{group(*regex)}\b"
