# Tokeniser Module
### The tokeniser takes a plaintext input and generates a list of parsable token objects.

## Files

__./src/vivid/tokeniser/__<br/>
├── [\_\_init__.py](./__init__.py)<br/>
│．└── _Initialises the module._<br/>
├── [lexer.py](./lexer.py)<br/>
│．└── _Tokenises source code using lexical analysis._<br/>
├── [regex.py](./regex.py)<br/>
│．└── _Contains methods that simplify regular expressions._<br/>
└── [tokens.py](./tokens.py)<br/>
．．└── _Stores token objects and regular expressions._<br/>

## Tokeniser Example
The tokeniser takes simple input code written in Vivid:
```rust
// main.vv
fn main() -> void {
    println("Hello, world!");
}
```

And outputs a list of significant tokens:
```
Token              Type                 Lexeme
----------------------------------------------
METHOD             KEYWORD              fn
IDENTIFIER         IDENTIFIER           main
PAREN_OPEN         SCOPE_OPEN           (
PAREN_CLOSE        SCOPE_CLOSE          )
RETURN_TYPE        SYNTAX               ->
TYPE_VOID          DATATYPE             void
CURLY_OPEN         SCOPE_OPEN           {
PRINT_LINE         BUILTIN              println
PAREN_OPEN         SCOPE_OPEN           (
STRING             STRING               "Hello, world!"
PAREN_CLOSE        SCOPE_CLOSE          )
SEMICOLON          SYNTAX               ;
CURLY_CLOSE        SCOPE_CLOSE          }
```
Whitespace and comment lexemes do not affect the code and are ignored.
