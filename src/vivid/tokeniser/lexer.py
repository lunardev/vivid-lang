"""This module provides methods to convert Vivid source code into parsable tokens."""

import re
import sys
from functools import partial
from typing import Iterator

from vivid.errors import LexerError, Position
from vivid.errors.exceptions import VividError
from vivid.tokeniser.regex import match_or
from vivid.tokeniser.tokens import Token, TokenEnum, TokenType

# Removed parser/AST code due to incompletion.
# from vivid.parser import generate_ast
# from vivid.tokeniser.tokens import TokenIterator


class Lexer:
    """Main lexical analysis class."""

    def __init__(self, source_code: str, file_name: str) -> None:
        """Initialise the `Lexer` class instance variables."""
        self.source_code = source_code
        self.file_name = file_name

        # Compile all token regular expressions.
        token_regex = (f"(?P<{i.name}>{i.regex})" for i in TokenEnum)
        self.regex = re.compile(match_or(*token_regex))

        # Define the tokenised position (used in error messages).
        self.position = 0
        self.position_previous = 0
        self.line = 0

    @staticmethod
    def get_token_store(name: str) -> TokenEnum:
        """Get a token's token type using the regex group name."""
        return next(i for i in TokenEnum if name == i.name)

    def token(self) -> Token | None:
        """Get the next token object found.

        Raises:
            LexerError: a token is not matched by any regular expression.
        """
        # Return None once the position has reached the end of the code.
        if self.position >= len(self.source_code):
            return None
        else:
            # Ignore any whitespace present in the code, except line feeds (\n).
            regex = r"[^ \r\t\f]"
            match = re.compile(regex).search(self.source_code, self.position)

        if match is None:
            return None
        else:
            # Find any regular expression matches in the code.
            self.position = match.start()
            match = self.regex.match(self.source_code, self.position)

        # No match means an untokenisable sequence of characters was provided.
        if match is None or (group := match.lastgroup) is None:
            # Create a position object an raise the `LexerError` exception.
            position = Position(self.position, self.position_previous, self.line)
            raise LexerError(self.source_code, self.file_name, position)
        else:
            # If a new line character is found, iterate the line count.
            if group == TokenEnum.NEW_LINE.name:
                self.line += 1
                self.position += 1
                self.position_previous = self.position

            position = Position(self.position, self.position_previous, self.line)
            # Create the token object that will be used in parsing.
            token = self.get_token_store(group)
            token_obj = Token(
                token=token,
                type=token.type,
                position=position,
                lexeme=match.group(group),
            )
            self.position = match.end()
            return token_obj

    def generate_tokens(self) -> Iterator[Token]:
        """
        Generate all tokens to the end of the source code.

        Yields:
            Token: the next token object found in the source code.
        """
        self.position = 0
        # Ignore comments and whitespace.
        insignificant = (TokenType.COMMENT, TokenType.WHTIESPACE)
        while token := self.token():
            if token.type in insignificant:
                continue

            yield token

    def debug_tokens(self) -> int:
        """Generate token objects and write the data to stdout for debugging purposes."""
        try:
            token_factory = partial(self.generate_tokens)

            # Iterate through a copy of the tokens to catch any exceptions.
            _ = [*token_factory()]
        except VividError as error:
            print(error.traceback(), file=sys.stderr)
            return 1
        else:
            tokens = token_factory()
            print("{:<18} {:<12} {}".format("Token", "Type", "\tLexeme"))
            print("-" * (18 + 12 + 16))
            print(*tokens, sep="\n")

            # Removed parser/AST code due to incompletion.
            # iterator = TokenIterator(tokens)
            # tree = generate_ast(self.source_code, self.file_name, iterator)
            # if tree is not None:
            #     print(tree)

            return 0
