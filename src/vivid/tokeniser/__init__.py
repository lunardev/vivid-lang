"""Tokenise source code into parsable tokens using lexical analysis."""

from vivid.tokeniser.lexer import Lexer
from vivid.tokeniser.regex import exact, group, match_any, match_or, maybe
from vivid.tokeniser.tokens import Token, TokenIterator, TokenEnum, TokenType

# The __all__ variable specifies what identifiers are accessible via a wildcard
# import (from vivid.tokeniser import *). This reduces the verbosity of import
# statements while maintaining a relatively clean global namespace. Even in
# cases where wildcard imports aren't utilised, it clearly defines what
# identifiers are intended to be accessed.
__all__ = [
    "Lexer",
    "exact",
    "group",
    "match_any",
    "match_or",
    "maybe",
    "Token",
    "TokenIterator",
    "TokenEnum",
    "TokenType",
]
