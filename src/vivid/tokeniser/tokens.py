"""
This module stores all lexer token objects in an enum.

A token enum makes defining special symbols for the Vivid language
significantly cleaner and easier to work with.
"""

from dataclasses import dataclass
from enum import Enum, auto
from typing import Iterator, Self

from vivid.errors import Position
from vivid.tokeniser.regex import exact, group


class TokenType(Enum):
    """Store various types of Token objects."""

    # Operator types.
    BINARY_OP   = auto()
    UNARY_OP    = auto()

    # "Whitespace" or ignorable types.
    WHTIESPACE  = auto()
    COMMENT     = auto()

    # Grammar/syntax types.
    SYNTAX      = auto()
    SCOPE_OPEN  = auto()
    SCOPE_CLOSE = auto()

    # Reserved identifier types.
    KEYWORD     = auto()
    BUILTIN     = auto()
    DATATYPE    = auto()
    IDENTIFIER  = auto()

    # language datatypes.
    FLOAT       = auto()
    INTEGER     = auto()
    BOOLEAN     = auto()
    STRING      = auto()
    NULL        = auto()

    def __str__(self) -> str:
        """Return string representation of the token type."""
        return f"{self.name}\t"


class TokenEnum(Enum):
    """
    Define the name, token type, and regular expression of each token.

    The order of the tokens matters, as the regular expressions are
    parsed top to bottom when the lexer tokenises the code.
    """

    # Support shebang lines and comments starting with "//".
    SHEBANG            = (TokenType.COMMENT,     r"^#![^\r\n]*")
    COMMENT            = (TokenType.COMMENT,     r"[\/]{2}[^\r\n]*")

    # Define reserved keywords.
    LET                = (TokenType.KEYWORD,     exact("let"))
    CONSTANT           = (TokenType.KEYWORD,     exact("const"))
    METHOD             = (TokenType.KEYWORD,     exact("fn"))
    RETURN             = (TokenType.KEYWORD,     exact("return"))
    IF                 = (TokenType.KEYWORD,     exact("if"))
    ELSE               = (TokenType.KEYWORD,     exact("else"))
    FOR_LOOP           = (TokenType.KEYWORD,     exact("for"))

    # Define built-in methods.
    PRINT              = (TokenType.BUILTIN,     exact("print"))
    PRINT_LINE         = (TokenType.BUILTIN,     exact("println"))

    # Define data types.
    TYPE_INTEGER       = (TokenType.INTEGER,     exact("integer"))
    TYPE_FLOAT         = (TokenType.FLOAT,       exact("float"))
    TYPE_BOOLEAN       = (TokenType.BOOLEAN,     exact("boolean"))
    TYPE_STRING        = (TokenType.STRING,      exact("string"))
    TYPE_VOID          = (TokenType.DATATYPE,    exact("void"))
    TYPE_NULL          = (TokenType.NULL,        exact("null"))

    # Define regex for bool, string, float, and integer data types.
    BOOLEAN            = (TokenType.BOOLEAN,     exact("true", "false"))
    STRING             = (TokenType.STRING,      group('".*?"', "'.*?'"))
    FLOAT              = (TokenType.FLOAT,       r"[+\-]?\d+\.\d+")
    INTEGER            = (TokenType.INTEGER,     r"[+\-]?\d+")
    IDENTIFIER         = (TokenType.IDENTIFIER,  r"[a-zA-Z_]\w*")

    # Define identifier assignment operators.
    PLUS_EQUAL         = (TokenType.BINARY_OP,   r"\+=")
    MINUS_EQUAL        = (TokenType.BINARY_OP,   "-=")
    MULTIPLY_EQUAL     = (TokenType.BINARY_OP,   r"\*=")
    DIVIDE_EQUAL       = (TokenType.BINARY_OP,   r"\/=")
    MODULO_EQUAL       = (TokenType.BINARY_OP,   "%=")
    AND_EQUAL          = (TokenType.BINARY_OP,   "&=")
    OR_EQUAL           = (TokenType.BINARY_OP,   r"\|=")
    XOR_EQUAL          = (TokenType.BINARY_OP,   r"\^=")
    SHIFT_LEFT_EQUAL   = (TokenType.BINARY_OP,   "[<]{2}=")
    SHIFT_RIGHT_EQUAL  = (TokenType.BINARY_OP,   "[>]{2}=")

    # Define equalities.
    ASSIGN             = (TokenType.BINARY_OP,   "<-")
    EQUALITY           = (TokenType.BINARY_OP,   "[=]{2}")
    INEQUALITY         = (TokenType.BINARY_OP,   "!=")

    # Define logical operators AND, OR, and NOT.
    LOGICAL_AND        = (TokenType.BINARY_OP,   "[&]{2}")
    LOGICAL_OR         = (TokenType.BINARY_OP,   r"[\|]{2}")
    LOGICAL_NOT        = (TokenType.UNARY_OP,    "[!]")

    # Define bitwise operators AND, OR, XOR, complement, and L/R shift.
    AND                = (TokenType.BINARY_OP,   "[&]")
    OR                 = (TokenType.BINARY_OP,   r"[\|]")
    XOR                = (TokenType.BINARY_OP,   r"[\^]")
    COMPLEMENT         = (TokenType.UNARY_OP,    "[~]")
    SHIFT_LEFT         = (TokenType.BINARY_OP,   "[<]{2}")
    SHIFT_RIGHT        = (TokenType.BINARY_OP,   "[>]{2}")

    # Definine comparison operators >, <, >=, and <=.
    GREATER_THAN_EQUAL = (TokenType.BINARY_OP,   ">=")
    LESS_THAN_EQUAL    = (TokenType.BINARY_OP,   "<=")
    GREATER_THAN       = (TokenType.BINARY_OP,   "[>]")
    LESS_THAN          = (TokenType.BINARY_OP,   "[<]")

    # Indicate the type the return value should be in a method.
    RETURN_TYPE        = (TokenType.SYNTAX,      "->")

    # Define arithmetic operators ++, --, +, -, *, /, and %.
    INCREMENT          = (TokenType.UNARY_OP,    "[+]{2}")
    DECREMENT          = (TokenType.UNARY_OP,    r"[\-]{2}")
    PLUS               = (TokenType.BINARY_OP,   "[+]")
    MINUS              = (TokenType.BINARY_OP,   r"[\-]")
    MULTIPLY           = (TokenType.BINARY_OP,   "[*]")
    DIVIDE             = (TokenType.BINARY_OP,   r"\/")
    MODULO             = (TokenType.BINARY_OP,   "[%]")

    # Define symbols that change the current scope.
    PAREN_OPEN         = (TokenType.SCOPE_OPEN,  r"\(")
    BRACKET_OPEN       = (TokenType.SCOPE_OPEN,  r"\[")
    CURLY_OPEN         = (TokenType.SCOPE_OPEN,  r"\{")
    PAREN_CLOSE        = (TokenType.SCOPE_CLOSE, r"\)")
    BRACKET_CLOSE      = (TokenType.SCOPE_CLOSE, r"\]")
    CURLY_CLOSE        = (TokenType.SCOPE_CLOSE, r"\}")

    # Define syntax symbols.
    SEMICOLON          = (TokenType.SYNTAX,      "[;]")
    COLON              = (TokenType.SYNTAX,      "[:]")
    COMMA              = (TokenType.SYNTAX,      "[,]")
    NEW_LINE           = (TokenType.WHTIESPACE,  "[\n]")

    def __init__(self, token_type: TokenType, regex: str) -> None:
        """Initialise enum attributes."""
        super().__init__()
        self.type = token_type
        self.regex = regex

    def __str__(self) -> str:
        """Return the regular expression of the token instance."""
        return f"{self.name}:{self.type!s}"


@dataclass(frozen=True)
class Token:
    """Stores variables for the type of token and the token's value."""

    token: TokenEnum
    type: TokenType
    position: Position
    lexeme: str

    def __eq__(self, __o: object) -> bool:
        """Check if an object is equal to a Token instance."""
        if isinstance(__o, type(self)):
            # Compare all attributes of each object.
            try:
                is_equal = vars(self) == vars(__o)
            except TypeError:
                is_equal = False
        else:
            # Return False if the object isn't a Token instance.
            is_equal = False

        return is_equal

    def __str__(self) -> str:
        """
        Return a readable string representation of the token.

        Return all attributes of the token as a string. This is mainly
        for debugging purposes.
        """
        return "{:<18} {:<12}\t{}".format(self.token.name, self.token.type, self.lexeme)


class TokenIterator:
    """Allows peeking at the next token without consuming it."""

    def __init__(self, generator: Iterator[Token]) -> None:
        """Define class instance attributes."""
        self.empty = False
        self.peek = None
        self.generator = generator
        try:
            self.peek = next(self.generator)
        except StopIteration:
            self.empty = True

    def __iter__(self) -> Self:
        """Use `TokenIterator` as an iterable object."""
        return self

    def __next__(self) -> Token | None:
        """Return the `self.peek attribute`, or raise `StopIteration` if empty."""
        if self.empty:
            raise StopIteration
        forward_token = self.peek
        try:
            self.peek = next(self.generator)
        except StopIteration:
            self.peek = None
            self.empty = True
        return forward_token
