# Terminal Module
### Provide a real-eval-print loop (REPL) for the Vivid language. Adds additional commands for ease of use as well as helpful code debugging information.

## Files

__./src/vivid/terminal/__<br/>
├── [\_\_init__.py](./__init__.py)<br/>
│．└── _Initialises the module._<br/>
├── [display.py](./display.py)<br/>
│．└── _Provide ANSI escapce code sequences for the terminal dispaly._<br/>
└── [repl.py](./repl.py)<br/>
．．└── _A read-eval-print loop that accepts user input._<br/>

## Example REPL output:
![Example REPL output](../../../assets/screenshots/repl.png)
