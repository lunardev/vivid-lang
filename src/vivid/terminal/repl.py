"""
Provide a real-eval-print loop (REPL) for the Vivid language.

Adds additional commands for ease of use as well as helpful code
debugging information.
"""

import itertools
import platform
import sys
from pathlib import Path

from colorama import Fore, Style

from vivid.terminal.display import Ansi, clear_display
from vivid.tokeniser import Lexer


class Repl:
    """Vivid read-eval-print loop."""

    # Get license file absolute path.
    license = Path(__file__).resolve().parents[3].joinpath("LICENSE")
    prompt = f"{Fore.LIGHTGREEN_EX}${Fore.RESET} "

    def __init__(self, **attributes: str) -> None:
        """Initialise command history and write shell banner to stdout."""
        self.attributes = attributes
        self.history: list[str] = []

        # Display REPL banner message in terminal window.
        print(f"Vivid {self.attributes.get('version', '')} REPL on {platform.system()}")
        print(f"Type {Fore.GREEN}(h)elp{Fore.RESET} for more information.", end=" ")
        print(f"Type {Fore.GREEN}(q)uit{Fore.RESET} to exit the REPL shell.")

    def show_help(self) -> None:
        """Display project information as well as REPL-only commands."""
        commands = {
            "h, help": "Display this message.",
            "q, quit": "Exit the REPL.",
            "license": "View the project license.",
            "history": "View the command history of the current REPL session.",
            "clear": "Clear the terminal display.",
        }

        # Print out a list of all project attributes and REPL commands.
        print(f"\t{Ansi.REVERSE}{Fore.LIGHTGREEN_EX} Help ")
        print(*(f"{k}\t: {v}" for k, v in self.attributes.items()), sep="\n")
        print(f"\n\t{Ansi.REVERSE}{Fore.LIGHTGREEN_EX} Commands ")
        print(*(f"{k}\t: {v}" for k, v in commands.items()), sep="\n")

    def show_license(self) -> None:
        """Display the MIT license/copyright notice of the project."""
        try:
            self.license.resolve(strict=True)
        except FileNotFoundError:
            print("License not found.")
        else:
            print(self.license.read_text())

    def loop(self) -> None:
        """Loop continously while evaluating user input."""
        for i in itertools.count():
            # Prompt the user for input.
            line_number = f"{Style.DIM}[{i + 1}]{Style.RESET_ALL}"
            command = input(f"{line_number} {self.prompt}").strip()
            # Add input to command history.
            self.history.append(command)

            # Ignore blank lines.
            if not command:
                del self.history[-1]
                continue

            # Match the command to pre-defined patterns.
            match command.lower():
                case "quit":
                    break
                case "q":
                    break
                case "help":
                    self.show_help()
                case "h":
                    self.show_help()
                case "license":
                    self.show_license()
                case "history":
                    # Print out a list of all commands used.
                    if self.history:
                        print(*self.history, sep="\n")
                    else:
                        print("No history saved.")
                case "clear":
                    clear_display()
                # `_` is the default pattern.
                case _:
                    # Display a list of tokens using lexical analysis.
                    lexer = Lexer(command, sys.stdin.name)

                    try:
                        lexer.debug_tokens()
                    except Exception as error:
                        print(error, file=sys.stderr)


def enter_repl(**kwargs: str) -> int:
    """Enter the REPL shell loop."""
    clear_display()
    shell = Repl(**kwargs)
    try:
        shell.loop()
    # `KeyboardInterrupt` is raised by Ctrl + C.
    except KeyboardInterrupt:
        print()

    # Return with an exit code of 0.
    print(f"{Fore.RED}Exiting the Vivid REPL process.")
    return 0
