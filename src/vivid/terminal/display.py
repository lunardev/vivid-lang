"""This module provides methods to improve the terminal display."""

from enum import Enum

from colorama.ansi import CSI


class Ansi(Enum):
    """
    Enum of ANSI Select Graphic Rendition (SGR) escape sequences.

    Refer to https://wikipedia.org/wiki/ANSI_escape_code#SGR.
    """

    ITALIC = f"{CSI}3m"
    UNDERLINE = f"{CSI}4m"
    REVERSE = f"{CSI}7m"
    CROSSED = f"{CSI}9m"

    def __str__(self) -> str:
        """Return ANSI escape sequence."""
        return self.value


def clear_display() -> None:
    """Clear the terminal display window using ANSI escape sequences."""
    print(end=f"{CSI}H{CSI}2J{CSI}3J")
