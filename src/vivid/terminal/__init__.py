"""Provide a real-eval-print loop (REPL) for the Vivid language."""

from vivid.terminal.display import Ansi
from vivid.terminal.repl import enter_repl

# The identifiers accessible via a wildcard import (from vivid.terminal import *).
__all__ = ["Ansi", "enter_repl"]
