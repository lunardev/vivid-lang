"""This module provides exception classes that are used by the Vivid language."""

from vivid.errors.exceptions import (
    BadTypeError,
    IllegalCharError,
    InvalidSyntaxError,
    LexerError,
    ParserError,
    Position,
    SymbolNotFoundError,
    VividError,
)

# The identifiers accessible via a wildcard import (from vivid.errors import *).
__all__ = [
    "BadTypeError",
    "IllegalCharError",
    "InvalidSyntaxError",
    "LexerError",
    "ParserError",
    "Position",
    "SymbolNotFoundError",
    "VividError",
]
