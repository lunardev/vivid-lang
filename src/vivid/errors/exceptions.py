"""This module provides exception classes that are used by the Vivid language."""

from os import get_terminal_size
from typing import NamedTuple

from colorama import Fore, Style


class Position(NamedTuple):
    """Store the current position values as a named tuple."""

    column: int
    previous: int
    line: int

    def __repr__(self) -> str:
        """Represent the Position object as a string."""
        return f"Position( line={f'{self.line},':<3} column={self.column - self.previous:<3})"


class VividError(Exception):
    """Common base class for all Vivid exceptions."""

    NO_TYPE = "no type annotation provided."
    BAD_TYPE = "cannot assign {} to variable of type {}."
    INCOMPLETE = "incomplete input."
    SEMICOLON = "missing semicolon at end of statement."

    def __init__(self, src: str, file: str, position: Position, message: str) -> None:
        """Initialise the exception instance variables."""
        super().__init__()
        self.src = src
        self.file = file
        self.position = position
        self.message = message

        # Add one to an index to get the actual count (indexes start at 0).
        self.index = 1
        # Print `n` code lines on both sides of the line that caused the exception.
        self.lines = 3

    def __repr__(self) -> str:
        """Return the name of the exception."""
        return type(self).__name__

    def __str__(self) -> str:
        """Return the exception message."""
        return self.message

    def traceback(self) -> str:
        """Return a traceback string of the exception."""
        traceback_status = "Traceback (most recent call last)"
        column = self.position.column - self.position.previous
        src_lines = self.src.splitlines()

        # Find the first line of code to print in the exception.
        first_line = max(self.position.line - self.lines, 0)
        # Find the last line of code to print in the exception.
        last_line = min(self.position.line + self.lines + self.index, len(src_lines))
        # Split the lines of the code to get a range of lines to display.
        line_content = src_lines[first_line:last_line]
        # Define padding of the line count.
        line_pad = len(str(last_line)) + self.index
        # The pointer will display an arrow at the character that caused the exception.
        exception_pointer = (
            f"{(column + line_pad + self.index * 2) * ' '}{Fore.LIGHTYELLOW_EX}▲{Fore.RESET}\n"
        )

        # Define the traceback string.
        column_count = get_terminal_size().columns
        message = "-" * column_count
        message += "\n"
        message += " " * (column_count - len(traceback_status))
        message += f"{Style.DIM}{traceback_status}{Style.RESET_ALL}"
        # Show the line count and column count of the exception.
        message += f"\rFile {Fore.LIGHTGREEN_EX}{self.file}{Fore.RESET}, Ln {self.position.line + self.index}, Col {column + self.index}:\n"
        # Iterate over each line of code and display the code traceback.
        for index, line in zip(range(first_line, last_line), line_content):
            # Highlight the line that caused the exception.
            ansi, pointer = (
                (Fore.LIGHTYELLOW_EX, exception_pointer)
                if line == src_lines[self.position.line]
                else (Style.DIM, "")
            )
            message += f"{ansi}{index+1:>{line_pad}}{Style.RESET_ALL}  {line}\n"
            message += pointer

        message += f"\n{Fore.LIGHTRED_EX}{self!r}{Fore.RESET}: {self!s}"
        return message


class LexerError(VividError):
    """Failed to tokenise an unknown token."""

    # Error message to be displayed if an exception is raised.
    message = "failed to tokenise invalid character sequence."

    def __init__(self, src: str, file: str, position: Position) -> None:
        """Pass variables to the base class."""
        super().__init__(src, file, position, self.message)


class ParserError(VividError):
    """Failed to parse sequence of tokens due to invalid syntax."""

    message = "failed to parse invalid syntax."

    def __init__(self, src: str, file: str, position: Position) -> None:
        """Pass variables to the base class."""
        super().__init__(src, file, position, self.message)


class InvalidSyntaxError(VividError):
    """Failed to parse invalid syntax."""

    def __init__(self, src: str, file: str, position: Position, message: str) -> None:
        """Pass variables to the base class."""
        super().__init__(src, file, position, message)


class BadTypeError(VividError):
    """Failed to parse type."""

    def __init__(self, src: str, file: str, position: Position, message: str) -> None:
        """Pass variables to the base class."""
        super().__init__(src, file, position, message)


# The following exception classes are unused:


class IllegalCharError(VividError):
    """Provided character is invalid."""

    message = "character {!r} is not supported."

    def __init__(self, src: str, file: str, position: Position, char: str) -> None:
        """Pass variables to the base class."""
        super().__init__(src, file, position, self.message.format(char))


class SymbolNotFoundError(VividError):
    """Called symbol does not exist."""

    message = "symbol '{}' was not found."

    def __init__(self, src: str, file: str, position: Position, symbol: str) -> None:
        """Pass variables to the base class."""
        super().__init__(src, file, position, self.message.format(symbol))
