# Errors Module
### Provides exception classes used by the Vivid language.

## Files

__./src/vivid/errors/__<br/>
├── [\_\_init__.py](./__init__.py)<br/>
│．└── _Initialises the module._<br/>
└── [exceptions.py](./exceptions.py)<br/>
．．└── _Define any custom exception classes used by the program._<br/>

## Example exception output:
![Example exception output](../../../assets/screenshots/exception.png)
